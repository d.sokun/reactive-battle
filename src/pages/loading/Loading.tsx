import React, { FC } from 'react';

import './loading.scss';

const Loading: FC = () => (
  <div className="loading" />
);

export default Loading;
